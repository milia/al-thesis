# Active Machine Learning

# Purpose

The purpose of this repository is primarily for storage and later when it's completed, to share my MSc thesis.
Since I'm obligated to write it in Greek, I'm intending to use this README as a synopsis of the main methods used, and the results found.

# Contents

## Tex

In this directory will lie the main body of my thesis. The detailed contents of the thesis will be added.

## Experiments

The files (arff, cfg) of the experiments run for the thesis, along with the results (report txt files) can be found in [this](https://bitbucket.org/milia/thesis-experiments) seperate repository.

## Active learning strategies

## Classifiers studied

## Main results

# References

Most important references are the following. Please see the thesis for the complete list.

1. Settles B., "Curious Machines: Active Learning with structured instances", Phd thesis, 2008
2. Settles B., "Active Learning Literature Survey", technical report, 2010
3. Settles B., "Active Learning", Synthesis Lectures on Artificial Intelligence and Machine Learning, Morgan & Claypool Publishers, 2010.
4. Oscar Reyes Eduardo Perez, Maria del Carmen Rodrigues-Hernandez, Habib M. Fardoun, Sebastian Ventura "JCLAL: A Java Framework for Active Learning", Version 1.1, User Manual and Developer Documentation, 2016


