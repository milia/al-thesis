\begin{thebibliography}{10}

\bibitem{Yasser}
{{\latintext Yaser{ }Abu-Mostafa}}.
\newblock {{\latintext Learning from data}}.
\newblock {{\latintext \url{http://work.caltech.edu/telecourse.html}}}, q.q.

\bibitem{Alpaydin:2014}
{{\latintext Ethem{ }Alpaydin}}.
\newblock {{\latintext {\em Introduction to Machine Learning}}}.
\newblock {{\latintext The MIT Press}}, 2014.

\bibitem{Angluin1988}
{{\latintext Dana{ }Angluin}}.
\newblock {{\latintext {Queries and Concept Learning}}}.
\newblock {{\latintext {\em Machine Learning}}}, 2(4):319--342, 1988.

\bibitem{atkinson1992optimum}
{{\latintext A. C.{ }Atkinson \textgreek{kai} A. N.{ }Donev}}.
\newblock {{\latintext Optimum experimental designs: Oxford science
  publications}}, 1992.

\bibitem{atlas1990training}
{{\latintext Les E.{ }Atlas, David A.{ }Cohn \textgreek{kai} Richard E.{
  }Ladner}}.
\newblock {{\latintext Training connectionist networks with queries and
  selective sampling}}.
\newblock {{\latintext \textgreek{Sto} {\em Advances in neural information
  processing systems}}}, \textgreek{sel'ides} 566--573, 1990.

\bibitem{baum1992query}
{{\latintext Eric B{ }Baum \textgreek{kai} Kenneth{ }Lang}}.
\newblock {{\latintext Query learning can work poorly when a human oracle is
  used}}.
\newblock {{\latintext \textgreek{Sto} {\em International joint conference on
  neural networks}}}, \textgreek{t'omos}\nobreakspace{}8,
  \textgreek{sel'ida}\nobreakspace{}8, 1992.

\bibitem{Cohn1994}
{{\latintext David{ }Cohn, Les{ }Atlas \textgreek{kai} Richard{ }Ladner}}.
\newblock {{\latintext Improving generalization with active learning}}.
\newblock {{\latintext {\em Machine Learning}}}, 15(2):201--221,   1994.

\bibitem{Cohn:1996}
{{\latintext David A.{ }Cohn, Zoubin{ }Ghahramani \textgreek{kai} Michael I.{
  }Jordan}}.
\newblock {{\latintext Active learning with statistical models}}.
\newblock {{\latintext {\em J. Artif. Int. Res.}}}, 4(1):129--145,   1996.

\bibitem{dasgupta2011two}
{{\latintext Sanjoy{ }Dasgupta}}.
\newblock {{\latintext Two faces of active learning}}.
\newblock {{\latintext {\em Theoretical computer science}}},
  412(19):1767--1781, 2011.

\bibitem{dasgupta2008hierarchical}
{{\latintext Sanjoy{ }Dasgupta \textgreek{kai} Daniel{ }Hsu}}.
\newblock {{\latintext Hierarchical sampling for active learning}}.
\newblock {{\latintext \textgreek{Sto} {\em Proceedings of the 25th
  international conference on Machine learning}}}, \textgreek{sel'ides}
  208--215. {{\latintext ACM}}, 2008.

\bibitem{dos2015selectively}
{{\latintext Davi P{ }Dos~Santos \textgreek{kai} Andr{\'e} CPLFde{ }Carvalho}}.
\newblock {{\latintext Selectively inhibiting learning bias for active
  sampling}}.
\newblock {{\latintext \textgreek{Sto} {\em Intelligent Systems (BRACIS), 2015
  Brazilian Conference on}}}, \textgreek{sel'ides} 62--67. {{\latintext IEEE}},
  2015.

\bibitem{elahi2016survey}
{{\latintext Mehdi{ }Elahi, Francesco{ }Ricci \textgreek{kai} Neil{ }Rubens}}.
\newblock {{\latintext A survey of active learning in collaborative filtering
  recommender systems}}.
\newblock {{\latintext {\em Computer Science Review}}}, 20:29--50, 2016.

\bibitem{Eremenko}
{{\latintext Kirill{ }Eremenko}}.
\newblock {{\latintext Machine learning a-z}}.
\newblock {{\latintext \url{https://www.udemy.com/machinelearning/}}}, q.q.

\bibitem{fedorov1972theory}
{{\latintext Valerii V.{ }Fedorov}}.
\newblock {{\latintext {\em Theory of optimal experiments}}}.
\newblock {{\latintext Elsevier}}, 1972.

\bibitem{garcia2008extension}
{{\latintext Salvador{ }Garcia \textgreek{kai} Francisco{ }Herrera}}.
\newblock {{\latintext An extension on``statistical comparisons of classifiers
  over multiple data sets''for all pairwise comparisons}}.
\newblock {{\latintext {\em Journal of Machine Learning Research}}},
  9(Dec):2677--2694, 2008.

\bibitem{guo2007optimistic}
{{\latintext Yuhong{ }Guo \textgreek{kai} Russell{ }Greiner}}.
\newblock {{\latintext Optimistic active-learning using mutual information.}}
\newblock {{\latintext \textgreek{Sto} {\em IJCAI}}},
  \textgreek{t'omos}\nobreakspace{}7, \textgreek{sel'ides} 823--829, 2007.

\bibitem{ho1995random}
{{\latintext Tin Kam{ }Ho}}.
\newblock {{\latintext Random decision forests}}.
\newblock {{\latintext \textgreek{Sto} {\em Document Analysis and Recognition,
  1995., Proceedings of the Third International Conference on}}},
  \textgreek{t'omos}\nobreakspace{}1, \textgreek{sel'ides} 278--282.
  {{\latintext IEEE}}, 1995.

\bibitem{kearns1994toward}
{{\latintext Michael J{ }Kearns, Robert E{ }Schapire \textgreek{kai} Linda M{
  }Sellie}}.
\newblock {{\latintext Toward efficient agnostic learning}}.
\newblock {{\latintext {\em Machine Learning}}}, 17(2-3):115--141, 1994.

\bibitem{Kotsiantis07}
{{\latintext S. B.{ }Kotsiantis}}.
\newblock {{\latintext Supervised machine learning: A review of classification
  techniques. informatica 31:249–268}}.
\newblock 2007.

\bibitem{kremer2014active}
{{\latintext Jan{ }Kremer, Kim{ }Steenstrup~Pedersen \textgreek{kai} Christian{
  }Igel}}.
\newblock {{\latintext Active learning with support vector machines}}.
\newblock {{\latintext {\em Wiley Interdisciplinary Reviews: Data Mining and
  Knowledge Discovery}}}, 4(4):313--326, 2014.

\bibitem{Geogebra.Lacoste}
{{\latintext J.{ }Lacoste}}.
\newblock {{\latintext 3d geogebra sketches}}.
\newblock {{\latintext \url{https://www.geogebra.org/b/PPXh5CGM}}}, q.q.

\bibitem{lewis1994sequential}
{{\latintext David D{ }Lewis \textgreek{kai} William A{ }Gale}}.
\newblock {{\latintext A sequential algorithm for training text classifiers}}.
\newblock {{\latintext \textgreek{Sto} {\em Proceedings of the 17th annual
  international ACM SIGIR conference on Research and development in information
  retrieval}}}, \textgreek{sel'ides} 3--12. {{\latintext Springer-Verlag New
  York, Inc.}}, 1994.

\bibitem{Mitchell1997machine}
{{\latintext Tom M.{ }Mitchell}}.
\newblock {{\latintext {\em Machine learning}}}.
\newblock {{\latintext McGraw Hill}}, 1997.

\bibitem{pereira2017empirical}
{{\latintext Davi{ }Pereira-Santos, Ricardo Bastos Cavalcante{ }Prud{\^e}ncio
  \textgreek{kai} Andr{\'e} CPLFde{ }Carvalho}}.
\newblock {{\latintext Empirical investigation of active learning strategies}}.
\newblock {{\latintext {\em Neurocomputing}}}, 2017.

\bibitem{Platt:1999}
{{\latintext John C.{ }Platt}}.
\newblock {{\latintext Using analytic qp and sparseness to speed training of
  support vector machines}}.
\newblock {{\latintext \textgreek{Sto} {\em Proceedings of the 1998 Conference
  on Advances in Neural Information Processing Systems II}}},
  \textgreek{sel'ides} 557--563, {{\latintext Cambridge, MA, USA}}, 1999.
  {{\latintext MIT Press}}.

\bibitem{Quinlan1993}
{{\latintext Ross{ }Quinlan}}.
\newblock {{\latintext {\em C4.5: Programs for Machine Learning}}}.
\newblock {{\latintext Morgan Kaufmann Publishers}}, {{\latintext San Mateo,
  CA}}, 1993.

\bibitem{reyes2016jclal}
{{\latintext Oscar{ }Reyes, Eduardo{ }P{\'e}rez, Mar{\'\i}a{ }Del Carmen
  Rodr{\'\i}guez-Hern{\'a}ndez, Habib M{ }Fardoun \textgreek{kai}
  Sebasti{\'a}n{ }Ventura}}.
\newblock {{\latintext Jclal: a java framework for active learning}}.
\newblock {{\latintext {\em The Journal of Machine Learning Research}}},
  17(1):3271--3275, 2016.

\bibitem{Settles2008}
{{\latintext Burr{ }Settles}}.
\newblock {{\latintext {\em {Curious machines: Active Learning with structured
  instances}}}}.
\newblock {{\latintext \textgreek{Didaktorik'h Diatrib'h} }}, 2008.

\bibitem{Settles2010}
{{\latintext Burr{ }Settles}}.
\newblock {{\latintext {Active Learning Literature Survey}}}.
\newblock {{\latintext {\em Machine Learning}}}, 15(2):201--221, 2010.

\bibitem{SettlesPresent}
{{\latintext Burr{ }Settles}}.
\newblock {{\latintext Active learning}}.
\newblock {{\latintext
  \url{https://www.cs.cmu.edu/~tom/10701_sp11/slides/settles-2.active.pdf}}},
  2011.

\bibitem{Settles2012}
{{\latintext Burr{ }Settles}}.
\newblock {{\latintext {\em {Active learning}}}},
  \textgreek{t'omos}\nobreakspace{}11.
\newblock {{\latintext Morgan \& Claypool Publishers}}, 2012.

\bibitem{settles2008active}
{{\latintext Burr{ }Settles, Mark{ }Craven \textgreek{kai} Lewis{ }Friedland}}.
\newblock {{\latintext Active learning with real annotation costs}}.
\newblock {{\latintext \textgreek{Sto} {\em Proceedings of the NIPS workshop on
  cost-sensitive learning}}}, \textgreek{sel'ides} 1--10, 2008.

\bibitem{sivaraman2014active}
{{\latintext Sayanan{ }Sivaraman \textgreek{kai} Mohan M{ }Trivedi}}.
\newblock {{\latintext Active learning for on-road vehicle detection: A
  comparative study}}.
\newblock {{\latintext {\em Machine vision and applications}}},
  \textgreek{sel'ides} 1--13, 2014.

\bibitem{Tan:2005}
{{\latintext Pang Ning{ }Tan, Michael{ }Steinbach \textgreek{kai} Vipin{
  }Kumar}}.
\newblock {{\latintext {\em Introduction to Data Mining, (First Edition)}}}.
\newblock {{\latintext Addison-Wesley Longman Publishing Co., Inc.}},
  {{\latintext Boston, MA, USA}}, 2005.

\bibitem{tong2001support}
{{\latintext Simon{ }Tong \textgreek{kai} Daphne{ }Koller}}.
\newblock {{\latintext Support vector machine active learning with applications
  to text classification}}.
\newblock {{\latintext {\em Journal of machine learning research}}},
  2(Nov):45--66, 2001.

\bibitem{Vapnik:1995}
{{\latintext Vladimir N.{ }Vapnik}}.
\newblock {{\latintext {\em The Nature of Statistical Learning Theory}}}.
\newblock {{\latintext Springer-Verlag New York, Inc.}}, {{\latintext New York,
  NY, USA}}, 1995.

\bibitem{Vapnik:1999}
{{\latintext Vladimir N.{ }Vapnik}}.
\newblock {{\latintext An overview of statistical learning theory}}.
\newblock {{\latintext {\em Trans. Neur. Netw.}}}, 10(5):988--999,   1999.

\bibitem{Winston:svm}
{{\latintext Patrick H.{ }Winston}}.
\newblock {{\latintext Artificial intelligence}}.
\newblock {{\latintext \url{http://ocw.mit.edu/6-034F10}}}, q.q.

\bibitem{Wu:2007}
{{\latintext Xindong{ }Wu, Vipin{ }Kumar, J.{ }Ross~Quinlan, Joydeep{ }Ghosh,
  Qiang{ }Yang, Hiroshi{ }Motoda, Geoffrey J.{ }McLachlan, Angus{ }Ng, Bing{
  }Liu, Philip S.{ }Yu, Zhi Hua{ }Zhou, Michael{ }Steinbach, David J.{ }Hand
  \textgreek{kai} Dan{ }Steinberg}}.
\newblock {{\latintext Top 10 algorithms in data mining}}.
\newblock {{\latintext {\em Knowl. Inf. Syst.}}}, 14(1):1--37,   2007.

\bibitem{zhu2003combining}
{{\latintext Xiaojin{ }Zhu, John{ }Lafferty \textgreek{kai} Zoubin{
  }Ghahramani}}.
\newblock {{\latintext Combining active learning and semi-supervised learning
  using gaussian fields and harmonic functions}}.
\newblock {{\latintext \textgreek{Sto} {\em ICML 2003 workshop on the continuum
  from labeled to unlabeled data in machine learning and data mining}}},
  \textgreek{t'omos}\nobreakspace{}3, 2003.

\end{thebibliography}
