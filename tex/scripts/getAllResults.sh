head General*EntropySampling* | grep "==>" && tail -n 25 General*EntropySampling* | grep "Correctly Classified Instances"
head General*Least* | grep "==>" && tail -n 25 General*Least* | grep "Correctly Classified Instances"
head General*Margin* | grep "==>" && tail -n 25 General*Margin* | grep "Correctly Classified Instances"
head General*VoteEntropy* | grep "==>" && tail -n 25 General*VoteEntropy* | grep "Correctly Classified Instances"
head General*Kullback* | grep "==>" && tail -n 25 General*Kullback* | grep "Correctly Classified Instances"
head General*RandomSelection* | grep "==>" && tail -n 25 General*RandomSelection* | grep "Correctly Classified Instances"

