\beamer@endinputifotherversion {3.36pt}
\select@language {english}
\beamer@sectionintoc {1}{Πρόβλημα}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{Το σύστημα}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Στόχοι ενός trader}{8}{0}{1}
\beamer@sectionintoc {2}{Δεδομένα}{12}{0}{2}
\beamer@subsectionintoc {2}{1}{Τι δεδομένα;}{12}{0}{2}
\beamer@subsectionintoc {2}{2}{Από αρχείο CSV}{19}{0}{2}
\beamer@subsectionintoc {2}{3}{Από το WWW}{20}{0}{2}
\beamer@sectionintoc {3}{Πρόβλεψη}{21}{0}{3}
\beamer@subsectionintoc {3}{1}{Τι και πώς}{21}{0}{3}
\beamer@subsectionintoc {3}{2}{Το μοντέλο}{25}{0}{3}
\beamer@subsectionintoc {3}{3}{Μαθηματικά εργαλεία}{31}{0}{3}
\beamer@sectionintoc {4}{Αξιολόγηση}{34}{0}{4}
\beamer@subsectionintoc {4}{1}{Κριτήρια}{34}{0}{4}
\beamer@subsectionintoc {4}{2}{Προσομοίωση M. Carlo}{36}{0}{4}
\beamer@subsectionintoc {4}{3}{Αποτελέσματα}{40}{0}{4}
\beamer@sectionintoc {5}{Επενδύσεις}{41}{0}{5}
\beamer@subsectionintoc {5}{1}{Αποτελέσματα}{41}{0}{5}
\beamer@sectionintoc {6}{Επίλογος}{43}{0}{6}
\beamer@subsectionintoc {6}{1}{Μερικές σκέψεις}{43}{0}{6}
\beamer@subsectionintoc {6}{2}{Ερωτήσεις}{49}{0}{6}
\beamer@subsectionintoc {6}{3}{Αναφορές}{50}{0}{6}
